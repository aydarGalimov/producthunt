//
//  Feed+DataSource.swift
//  ProductHunt
//
//  Created by Галимов Айдар on 17.08.17.
//  Copyright © 2017 Галимов Айдар. All rights reserved.
//

import UIKit
import SDWebImage
import NukeFLAnimatedImagePlugin
import Nuke

//MARK: Types

private enum FeedTableViewCellName: String {
    case productCell = "FeedCell"
}

private enum FeedIdentifier: String {
    case productCell = "FeedCell"
}

private enum DataSourceState {
    case empty
    case `default`
}

//MARK: - FeedTableViewDataSource: NSObject

final class FeedTableViewDataSource: NSObject {
    
    //MARK: Properties
    
    fileprivate var dataSourceState = DataSourceState.default
    internal var products: [Product]?
    
    //MARK: Public
    
    /// Returns product, that has certain index path
    internal func product(for indexPath: IndexPath) -> Product? {
        return products?[indexPath.row]
    }
}



//MARK: - UITableViewDataSource -

extension FeedTableViewDataSource: UITableViewDataSource {
    
    /// Return number of sections of tableView
    func numberOfSections(in tableView: UITableView) -> Int {
        let isEmpty = ((products == nil) || products?.count == 0)
        dataSourceState = isEmpty ? .empty : .default
        configure(tableView)
        return isEmpty ? 0 : 1
    }
    
    /// Return number of rows in section of tableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products?.count ?? 0
    }
    
    /// Configure and return cell of tableView
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FeedIdentifier.productCell.rawValue) as! FeedCell
        SDWebImageDownloader.shared().maxConcurrentDownloads = 10
        configure(cell, at: indexPath)
        return cell
    }
    
    
    //MARK: Configure
    
    /// Configure tableView, depending on dataSource state
    private func configure(_ tableView: UITableView) {
        switch dataSourceState {
        case .default:
            tableView.backgroundView = nil
            tableView.separatorStyle = .none
            tableView.estimatedRowHeight = 120
            tableView.register(UINib(nibName: FeedTableViewCellName.productCell.rawValue, bundle: nil), forCellReuseIdentifier: FeedIdentifier.productCell.rawValue)
        case .empty:
            let messageLabel = UILabel(frame: CGRect(origin: CGPoint.zero, size: tableView.bounds.size))
            messageLabel.text = "Nothing to show"
            messageLabel.numberOfLines = 1
            messageLabel.textAlignment = .center
            messageLabel.font = .systemFont(ofSize: 18)
            messageLabel.textColor = .lightGray
            messageLabel.sizeToFit()
            tableView.backgroundView = messageLabel
            tableView.separatorStyle = .none
        }
    }
    
    /// Configure cell
    private func configure(_ cell: FeedCell, at indexPath: IndexPath) {
        guard let selectedProduct = product(for: indexPath) else {return}
        
        cell.productNameLabel.text = selectedProduct.name
        cell.productDescriptionLabel.text = selectedProduct.description
        cell.productUpvotesNumberLabel.text = "▲ " + "\(selectedProduct.numberOfUpvotes)"
        cell.productThumbnailImage.image = UIImage(named: "logo")
        AnimatedImage.manager.loadImage(with: selectedProduct.thumbnailURL, into: cell.productThumbnailImage)
    }
}

//
//  ViewController.swift
//  ProductHunt
//
//  Created by Галимов Айдар on 17.08.17.
//  Copyright © 2017 Галимов Айдар. All rights reserved.
//

import UIKit
import BTNavigationDropdownMenu

final class FeedViewController: UIViewController {
    

    //MARK: - Outlets
    
    @IBOutlet var feedTableView: UITableView!
    
    
    //MARK: - Properties
    
    internal var currentCategory: Category?
    internal var downloadedCategories = [Category]()
    internal var downloadedProducts = [Product]()
    internal let tableViewDataSource = FeedTableViewDataSource()
    /// Services
    private var productDataProvider: ProductDataProvider!
    private var categoryDataProvider: CategoryDataProvider!
    /// Views
    internal var splashView = UIView()
    internal let refreshControl = UIRefreshControl()
    internal var dropDownMenu: BTNavigationDropdownMenu!
    /// Colors
    internal let productHuntColor = UIColor(red: 218.0/255.0, green: 84.0/255.0, blue: 47.0/255.0, alpha: 1.0)
    
    
    //MARK: - Controller Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureController()
        loadCategories()
    }
    
    
    //MARK: - Configure Controller
    
    internal func configureController() {
        configureProviders()
        configureDropDownMenu()
        configureTableView()
        configureSplashView()
    }
    
    internal func configureProviders() {
        self.productDataProvider = ProductDataProvider(network: APIInteractor())
        self.categoryDataProvider = CategoryDataProvider(network: APIInteractor())
    }
    
    internal func configureTableView() {
        self.feedTableView.delegate = self
        self.feedTableView.dataSource = self.tableViewDataSource
        self.addRefreshControl()
    }

    
    //MARK: - Data
    
    internal func loadCategories() {
        self.categoryDataProvider.fetchAllCategories {
            result in
            
            if case .success(let categories) = result {
                self.downloadedCategories = categories
                if let category = categories.first {
                    self.currentCategory = category
                    self.updateDropDownMenu()
                    self.loadProducts(for: category)
                }
            }
            
            if case .failure(let error) = result {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    internal func loadProducts(for category: Category) {
        
        self.showSplashView()
        
        self.productDataProvider.fetchProducts(forCategory: category.slug) {
            result in
            
            self.hideSplashView()
            
            if case .success(let products) = result {
                self.downloadedProducts = products
                self.tableViewDataSource.products = self.downloadedProducts
                self.feedTableView.reloadData()
            }
            
            if case .failure(let error) = result {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    internal func refreshProducts(for category: Category) {
        
        self.productDataProvider.fetchProducts(forCategory: category.slug) {
            result in
            
            self.refreshControl.endRefreshing()
            
            if case .success(let products) = result {
                self.downloadedProducts = products
                self.tableViewDataSource.products = self.downloadedProducts
                self.feedTableView.reloadData()
            }
            
            if case .failure(let error) = result {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    internal func updateFeed() {
        guard let category = self.currentCategory else {return}
        
        refreshProducts(for: category)
    }
    
    
    //MARK: - Navigation
    
    internal func showProductPage(with product: Product) {
        let storyboard = UIStoryboard(name: "ProductPage", bundle: Bundle.main)
        let ppc = storyboard.instantiateViewController(withIdentifier: "ProductPage") as! ProductPageController
        ppc.product = product
        self.navigationController?.pushViewController(ppc, animated: true)
    }
}


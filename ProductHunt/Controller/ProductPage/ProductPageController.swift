//
//  ProductPageController.swift
//  ProductHunt
//
//  Created by Галимов Айдар on 17.08.17.
//  Copyright © 2017 Галимов Айдар. All rights reserved.
//

import UIKit
import SDWebImage
import NukeFLAnimatedImagePlugin

final class ProductPageController: UIViewController {
    
    
    //MARK: - Outlets
    
    @IBOutlet var screenShotImage: UIImageView!
    @IBOutlet var productNameLabel: UILabel!
    @IBOutlet var productDescriptionLabel: UILabel!
    @IBOutlet var productUpvotesLabel: UILabel!
    
    
    //MARK: - Properties
    
    internal var product: Product?
    
    
    //MARK: - Controller Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBarColor()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        configureProductDetails()
    }
    
    
    //MARK: - UI
    
    private func configureProductDetails() {
        guard let product = self.product else {return}
        
        self.productNameLabel.text = product.name
        self.productDescriptionLabel.text = product.description
        self.productUpvotesLabel.text = "▲ " + "\(product.numberOfUpvotes)"
        self.screenShotImage.image = UIImage(named: "productpage_screenshot_placeholder")
        AnimatedImage.manager.loadImage(with: product.screenshotURL, into: self.screenShotImage)
    }
    
    private func setNavigationBarColor() {
        self.navigationController?.navigationBar.tintColor = UIColor(red: 218.0/255.0, green: 84.0/255.0, blue: 47.0/255.0, alpha: 1.0)
    }
    
    
    //MARK: - Navigation
    
    @IBAction func getItButtonTapped(_ sender: Any) {
        guard let url = self.product?.discussionURL else {return}
        
        UIApplication.shared.open(url)
    }
}

//
//  Feed+UI.swift
//  ProductHunt
//
//  Created by Галимов Айдар on 18.08.17.
//  Copyright © 2017 Галимов Айдар. All rights reserved.
//

import UIKit
import BTNavigationDropdownMenu

extension FeedViewController {
    
    
    //MARK: - Splash View
    
    internal func configureSplashView() {
        self.splashView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        self.splashView.backgroundColor = UIColor.white
        
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.color = self.productHuntColor
        activityIndicator.startAnimating()
        activityIndicator.center = splashView.center
        
        self.splashView.addSubview(activityIndicator)
        self.view.addSubview(self.splashView)
    }
    
    internal func showSplashView() {
        self.splashView.isHidden = false
    }
    
    internal func hideSplashView() {
        self.splashView.isHidden = true
    }
    
    
    //MARK: - Refresh Control
    
    internal func addRefreshControl() {
        self.refreshControl.addTarget(self, action: #selector(updateFeed), for: .valueChanged)
        self.refreshControl.tintColor = self.productHuntColor
        self.feedTableView.addSubview(refreshControl)
    }
    
    
    //MARK: - DropDownMenu
    
    internal func configureDropDownMenu() {
        let items = ["Loading ..."]
        
        self.dropDownMenu = BTNavigationDropdownMenu(navigationController: self.navigationController,
                                                     containerView: self.view,
                                                     title: items.first!,
                                                     items: items)
        
        self.navigationItem.titleView = self.dropDownMenu
        self.dropDownMenu.menuTitleColor = self.productHuntColor
        self.dropDownMenu.didSelectItemAtIndexHandler = {(indexPath: Int) -> () in}
    }
    
    internal func updateDropDownMenu() {
        if !downloadedCategories.isEmpty {
            
            let items = self.downloadedCategories.map({$0.name})
            
            let menuView = BTNavigationDropdownMenu(navigationController: self.navigationController, containerView: self.view, title: items.first!, items: items)
            
            self.navigationItem.titleView = menuView
            
            menuView.didSelectItemAtIndexHandler = {(indexPath: Int) -> () in
                self.currentCategory = self.downloadedCategories[indexPath]
                self.loadProducts(for: self.downloadedCategories[indexPath])
            }
            
            menuView.cellBackgroundColor = self.productHuntColor
            menuView.cellTextLabelColor = UIColor.white
            menuView.cellSelectionColor = UIColor.black
            menuView.cellSeparatorColor = self.productHuntColor
            menuView.menuTitleColor = self.productHuntColor
            menuView.arrowImage = UIImage(named: "arrow")
            menuView.arrowPadding = 20.0
            menuView.arrowTintColor = self.productHuntColor
        }
    }
}

//
//  APIInteractor.swift
//  ProductHunt
//
//  Created by Галимов Айдар on 17.08.17.
//  Copyright © 2017 Галимов Айдар. All rights reserved.
//

import Foundation
import Alamofire
import UnboxedAlamofire

internal final class APIInteractor {
    
    private static let urlRoot = "https://api.producthunt.com/v1"
    private static let token = "591f99547f569b05ba7d8777e2e0824eea16c440292cce1f8dfb3952cc9937ff"
    
    //MARK: - Category
    
    internal func fetchAllCategories(completion: @escaping ((Result<[Category], Error>) -> Void)) {
        
        let urlString = APIInteractor.urlRoot + "/categories/" + "?access_token=" + APIInteractor.token
        guard let url = URL(string: urlString) else {return}
        
        Alamofire.request(url, method: .get).responseArray(keyPath: "categories") { (
            response: DataResponse<[Category]>) in
            
            guard let categories = response.result.value else {
                completion(Result.failure(error: response.error!))
                return
            }
            
            completion(Result.success(result: categories))
        }
    }
    
    //MARK: - Product
    
    internal func fetchProducts(forCategory name: String, completion: @escaping ((Result<[Product], Error>) -> Void)) {
        
        let urlString = APIInteractor.urlRoot + "/categories/" + "\(name)" + "/posts/" + "?access_token=" + APIInteractor.token
        guard let url = URL(string: urlString) else {return}

        Alamofire.request(url, method: .get).responseArray(keyPath: "posts") { (
            response: DataResponse<[Product]>) in
            
            guard let products = response.result.value else {
                completion(Result.failure(error: response.error!))
                return
            }
            
            completion(Result.success(result: products))
        }
    }
}


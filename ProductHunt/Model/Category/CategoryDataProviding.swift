//
//  CategoryDataProviding.swift
//  ProductHunt
//
//  Created by Галимов Айдар on 17.08.17.
//  Copyright © 2017 Галимов Айдар. All rights reserved.
//

import Foundation
import Unbox

internal protocol CategoryDataProviding {
    func fetchAllCategories(completion: @escaping ((Result<[Category], Error>) -> Void))
}

//
//  Category.swift
//  ProductHunt
//
//  Created by Галимов Айдар on 17.08.17.
//  Copyright © 2017 Галимов Айдар. All rights reserved.
//

import Foundation
import Unbox

internal struct Category {
    internal let id: Int
    internal let name: String
    internal let slug: String
}

//MARK: - Unboxable
extension Category: Unboxable {
    init(unboxer: Unboxer) throws {
        self.id = try unboxer.unbox(key: "id")
        self.name = try unboxer.unbox(key: "name")
        self.slug = try unboxer.unbox(key: "slug")
    }
}

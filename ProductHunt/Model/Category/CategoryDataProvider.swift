//
//  CategoryDataProvider.swift
//  ProductHunt
//
//  Created by Галимов Айдар on 17.08.17.
//  Copyright © 2017 Галимов Айдар. All rights reserved.
//

import Foundation

internal final class CategoryDataProvider: CategoryDataProviding {
    
    private let network: APIInteractor
    
    internal required init(network: APIInteractor) {
        self.network = network
    }
    
    internal func fetchAllCategories(completion: @escaping ((Result<[Category], Error>) -> Void)) {
        defer {
            self.network.fetchAllCategories {
                result in
                completion(result)
            }
        }
    }
}

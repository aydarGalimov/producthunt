//
//  Result.swift
//  ProductHunt
//
//  Created by Галимов Айдар on 17.08.17.
//  Copyright © 2017 Галимов Айдар. All rights reserved.
//

import Foundation

internal enum Result<TResult, TError> {
    case success(result: TResult)
    case failure(error: TError)
}

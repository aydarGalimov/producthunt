//
//  ProductDataProvider.swift
//  ProductHunt
//
//  Created by Галимов Айдар on 17.08.17.
//  Copyright © 2017 Галимов Айдар. All rights reserved.
//

import Foundation

internal final class ProductDataProvider: ProductDataProviding {
    
    private let network: APIInteractor
    
    internal required init(network: APIInteractor) {
        self.network = network
    }
    
    internal func fetchProducts(forCategory name: String, completion: @escaping ((Result<[Product], Error>) -> Void)) {
        defer {
            self.network.fetchProducts(forCategory: name) {
                result in
                completion(result)
            }
        }
    }
}

//
//  ProductDataProviding.swift
//  ProductHunt
//
//  Created by Галимов Айдар on 17.08.17.
//  Copyright © 2017 Галимов Айдар. All rights reserved.
//

import Foundation

internal protocol ProductDataProviding {
    func fetchProducts(forCategory name: String, completion: @escaping ((Result<[Product], Error>) -> Void))
}

//
//  Product.swift
//  ProductHunt
//
//  Created by Галимов Айдар on 17.08.17.
//  Copyright © 2017 Галимов Айдар. All rights reserved.
//

import Foundation
import Unbox

internal struct Product {
    internal let id: Int
    internal let name: String
    internal let description: String
    internal let numberOfUpvotes: Int
    internal let thumbnailURL: URL
    internal let screenshotURL: URL
    internal let discussionURL: URL?
}

//MARK: - Unboxable
extension Product: Unboxable {
    init(unboxer: Unboxer) throws {
        self.id = try unboxer.unbox(key: "id")
        self.name = try unboxer.unbox(key: "name")
        self.description = try unboxer.unbox(key: "tagline")
        self.numberOfUpvotes = try unboxer.unbox(key: "votes_count")
        self.thumbnailURL = try unboxer.unbox(keyPath: "thumbnail.image_url")
        self.screenshotURL = try unboxer.unbox(keyPath: "screenshot_url.300px")
        self.discussionURL = unboxer.unbox(key: "redirect_url")
    }
}





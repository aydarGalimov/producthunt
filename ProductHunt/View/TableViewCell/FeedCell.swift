//
//  FeedCell.swift
//  ProductHunt
//
//  Created by Галимов Айдар on 17.08.17.
//  Copyright © 2017 Галимов Айдар. All rights reserved.
//

import UIKit

class FeedCell: UITableViewCell {
    
    @IBOutlet var productThumbnailImage: UIImageView!
    @IBOutlet var productNameLabel: UILabel!
    @IBOutlet var productDescriptionLabel: UILabel!
    @IBOutlet var productUpvotesNumberLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.productThumbnailImage.layer.cornerRadius = 15
        self.productThumbnailImage.clipsToBounds = true
    }
}

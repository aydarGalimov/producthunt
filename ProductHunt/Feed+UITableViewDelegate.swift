//
//  Feed+UITableViewDelegate.swift
//  ProductHunt
//
//  Created by Галимов Айдар on 17.08.17.
//  Copyright © 2017 Галимов Айдар. All rights reserved.
//

import UIKit

extension FeedViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let product = self.downloadedProducts[indexPath.row]
        self.showProductPage(with: product)
        self.feedTableView.deselectRow(at: indexPath, animated: true)
    }
}
